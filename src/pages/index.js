import * as React from "react"

import Layout from "../components/layout"
import SEO from "../components/seo"

const IndexPage = () => (
  <Layout>
    <SEO title="Shell Banking Documentation" />
    <h2>About the project</h2>
    <p>
      Shell Bank is a bank application that allows both users and employees to
      work within the system. The users can access their bank accounts, transfer
      money to other accounts within the system while also requesting loans, if
      their rank is high enough. The employees can view customers within the
      system, delete them, change their rank, approve loans and create new loan
      types.
    </p>
    <h2>Running our app with Docker</h2>
    <p>
      In order to be able to run a dockerized version of our project on your
      machine you need to take these steps:
    </p>
    <ul>
      <li>
        Clone into a folder on your machine from this repository:{" "}
        <a href="https://gitlab.com/bestgroupever3/django-bank-app">Bank App</a>
      </li>
      <li>
        Using your terminal, you need to access the cloned project and move into
        it: “cd /your/folder”
      </li>
      <li>
        From the same terminal you must run our docker-compose.yml file using
        the following syntax:{" "}
        <span style={{ fontWeight: "700", display: "block" }}>
          “docker-compose up”
        </span>
      </li>
      <li>
        When the docker container is up and running you can open your browser
        and navigate to localhost:8000 where you will be able to see and use the
        application.
      </li>
    </ul>
  </Layout>
)

export default IndexPage
